// 1. How do you create arrays in JS?
 let array = []
// 2. How do you access the first character of an array?
 array[0]
// 3. How do you access the last character of an array?
 array[array.length-1]
// 4. What array method searches for, and returns the index of a given value in an array? 
// This method returns -1 if given value is not found in the array.
 indexOf()
// 5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
.forEach()
// 6.   What array method creates a new array with elements obtained from a user-defined function?
 .map()
// 7.   What array method checks if all its elements satisfy a given condition?
 .every()
// 8.   What array method checks if at least one of its elements satisfies a given condition?
 .some()
// 9.   True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
 False
// 10.   True or False: array.slice() copies elements from original array and returns these as a new array.
 True

