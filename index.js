

let students = [
		"Tony",
		"Peter",
		"Wanda",
		"Vision",
		"Loki"
	]

// 1.
    function addToEnd(students, name) {
	if (typeof(name) !== "string"){
		return('Error - Can only add strings to an Array')
	}else {
	    students.push(name)
	 return students
	}
}


//2.
	function addToStart(students, name)  {
		if (typeof(name) !== "string"){
			return 'Error - can only add strings to an Array'
		}else {
		    students.unshift(name)
		 return students
		}
}


// 3. 
	function elementChecker(array, element){
		if (array.length === 0){
			return 'Error - passed in Array is Empty'
		}else if (array.indexOf(element) === -1 ){
			return true
		}
		

	}

// 4. 
	function checkAllStringsEnding(array, char){
		if (array.length === 0)
			return 'Error - Array must NOT be empty'
		if (array.some(elem => typeof(elem) !== 'string')) 
			return 'Error - All array elements must be strings'
		if (typeof(char) !== 'string')
			return 'Error - 2nd argument must be of data type string'
		if (char.length > 1)
			return 'Error - 2nd argument must be a single character'
		else
			return array.every(elem => elem.endsWith(char))
		
	}

	
// 5.
	function stringLengthSorter(array){
		if (array.some(elem => typeof(elem) !== 'string')){
			return 'Error - All array elements must be strings'
		} else {
			return array.sort((a,b) => a.length - b.length)
		}
	}


// 6.
	function startsWithCounter(array, char){

	let count = 0	
		if (students.length === 0)
			return 'Error - Array must NOT be empty'
		if (typeof(array && char) !== 'string')
			return 'Error - All array elements must be strings'
		if  (typeof(char) !== 'string')
			return 'Error - 2nd argument must be of data type string'
		if (char.length > 1)
			return 'Error - 2nd argument must be a single character' 
		
		array.map(elem => {   
        if(elem.toLowerCase().startsWith(char.toLowerCase())) count += 1
    })
		return count
	}

// 7.
	function likeFinder(array, char){
		if (array.length === 0)
			return 'Error - Array must NOT be empty'
		if (array.some(elem => typeof(array && char) !== 'string'))
			return 'Error - All array elements must be strings'
		if  (typeof(char.toLowerCase()) !== 'string')
			return 'Error - 2nd argument must be of data type string'
		
		let like = []

	    array.forEach(elem => {
	        if(elem.startsWith(char)) like.push(elem)
	    })

	    return like
	}

// 8.
	function randomPicker(students){
		const random = Math.floor(Math.random() * students.length);
        console.log(students[random]);
	}

